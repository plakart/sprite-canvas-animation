# Anleitung

##### Vorraussetzungen

- jQuery (min. 1.11.3)

##### Markup

```html
<canvas id="animation"></canvas>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="spriteanimation.js"></script>
<script>
    $("#animation").spriteAnimation({
        image: 'sprite.jpg',
        images: 59,
        fps: 20,
        width: 500,
        height: 322.5
    });
</script>
```
##### Optionen

`image` Pfad zur Bilddatei

`images` Anzahl der Bilder

`fps` Frames per Second (duration)

`width` Breite des Bildes

`height` Höhe **eines** Bildes

##### Beispiel

http://plakart-hosting.de/projekte/sprite-animation/