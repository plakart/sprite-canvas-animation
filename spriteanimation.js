/*
**
** Sprite-Canvas-Animation
**
*/

;(function($){

    $.fn.spriteAnimation = function(options){

        // Optionen
        options = $.extend({
            image: '',
            images: 0,
            fps: 0,
            width: 0,
            height: 0,
        }, options);

        // Höhe + Breite
        var width = options.width;
        var height = options.height;

        // Bild
        var img = new Image();
        img.src = options.image;

        // Canvas
        var canvas = $(this);
        canvas.get(0).width = width;
        canvas.get(0).height = height;
        var ctx = canvas.get(0).getContext("2d");

        // Frames
        var frames = options.images-1;
        var curFrame = 0;

        function animate(){
            setTimeout(function() {
                requestAnimationFrame(animate);
                ctx.clearRect(0, 0, width, height);
                ctx.drawImage(img, 0, height * curFrame, width, height, 0, 0, width, height);
                if(curFrame == frames) {
                    curFrame = 0;
                } else {
                    curFrame++;
                }
            }, 1000 / options.fps);
        }

        animate();

    }

})(jQuery);